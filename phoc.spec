%global         wlroots_commit 7723a09a226d812aa51d1439c2b5e8e21238d45c
Name:		phoc
Version:	0.4.4
Release:	1%{?dist}
Summary:	Display compositor designed for phones

%define _unpackaged_files_terminate_build 0

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/phoc
%undefine _disable_source_fetch
Source0:        https://source.puri.sm/Librem5/phoc/-/archive/v%{version}/%{name}-v%{version}.tar.gz
%undefine _disable_source_fetch
Source1:        https://source.puri.sm/Librem5/wlroots/-/archive/%{wlroots_commit}/wlroots-%{wlroots_commit}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(gio-2.0) >= 2.50.0
BuildRequires:  pkgconfig(glib-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gobject-2.0) >= 2.50.0
BuildRequires:  pkgconfig(libinput)
BuildRequires:  pkgconfig(pixman-1)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-protocols) >= 1.15
BuildRequires:  pkgconfig(wayland-server)
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  pkgconfig(wlroots) >= 0.9.0
BuildRequires:  pkgconfig(gnome-desktop-3.0)

BuildRequires:  pkgconfig(egl)
BuildRequires:  pkgconfig(freerdp2)
BuildRequires:  pkgconfig(gbm) >= 17.1.0
BuildRequires:  pkgconfig(glesv2)
BuildRequires:  pkgconfig(libdrm) >= 2.4.95
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols) >= 1.16
BuildRequires:  pkgconfig(libcap)
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  pkgconfig(x11-xcb)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xcb-composite)
BuildRequires:  pkgconfig(xcb-icccm)
BuildRequires:  pkgconfig(xcb-image)
BuildRequires:  pkgconfig(xcb-render)
BuildRequires:  pkgconfig(xcb-xfixes)
BuildRequires:  pkgconfig(xcb-xkb)
BuildRequires:  pkgconfig(xcb-errors)


%description
Phoc is a wlroots based Phone compositor as used on the Librem5. Phoc is
pronounced like the English word fog.


%prep
%setup -a1 -q -n %{name}-v%{version}
rmdir subprojects/wlroots
ln -s ../wlroots-%{wlroots_commit} subprojects/wlroots
cd subprojects/wlroots
for i in $(find debian/patches/ -name "*.patch"); do patch -p1 < $i; done

%build
%meson #-Dembed-wlroots=disabled  
%meson_build


%install
%meson_install

%files
%{_bindir}/phoc
%{_datadir}/glib-2.0/schemas/sm.puri.phoc.gschema.xml
%doc README.md
%license COPYING

%changelog
